const request = require('request-promise');
const TelegramBot = require('node-telegram-bot-api');
const fs = require('fs');

let configJson = fs.readFileSync('config.json');
let config = JSON.parse(configJson);

const bot = new TelegramBot(config.tg_token, {polling: true});

function getHttpCallBySentence(sentenceAsked) {
  let talkJson = fs.readFileSync('talk.json');
  let talk = JSON.parse(talkJson);
  // console.log(talk);
  for (key in talk) {
    for (var i = 0; i < talk[key].sentences.length; i++) {
      for (var j = 0; j < talk[key].sentences[i].sentence.length; j++) {
        if (sentenceAsked == talk[key].sentences[i].sentence[j]) {

          for (var k = 0; k < talk[key].actions.length; k++) {
            if (talk[key].actions[k].name == talk[key].sentences[i].action) {
              var alea = Math.random() * eval(talk[key].actions[k].response.length-1);
              alea = Math.round(alea);

              var wait_response;
              if (talk[key].actions[k].waiting_response) {
                wait_response = talk[key].actions[k].waiting;
              } else {
                wait_response = null;
              }

              var response = {
                sentence: talk[key].actions[k].response[alea],
                url: talk[key].actions[k].call,
                type: talk[key].actions[k].type,
                waiting:wait_response,
                found:true
              };

              return response;
            }
          }

        }
      }
    }
  }

  var response = {
    sentence: "No comprendo",
    url: null,
    type: null,
    waiting: null,
    found:false
  };

  return response;
};

function launchAction(callOptions) {
  const options = {
    method: callOptions.type,
    uri: config.base_url + callOptions.url,
    headers: {
      'User-Agent': 'Request-Promise',
      'Authorization': 'Bearer ' + config.hassio_token,
      'Content-Type': 'application/json'
    },
    json:true
  };

  return request(options)
  .then(function (response) {
    if (callOptions.waiting != null) {
      return response[callOptions.waiting];
    }
  })
  .catch(function (err) {
    console.log(err);
  });
};

function createSentence(sentenceOptions) {
  if (sentenceOptions.more != null) {
    return sentenceOptions.sentence.replace("{{x}}", sentenceOptions.more);
  } else {
    return sentenceOptions.sentence;
  }
};

// Ecoute tous les messages et reagit (ou pas) en fonction
bot.on('message', (msg) => {

  var callOptions = getHttpCallBySentence(msg.text);
  if (callOptions.found && callOptions.url != null) {
    var response = launchAction(callOptions).then(result => {
      var sentenceOptions = {
        sentence: callOptions.sentence,
        more: result
      }
      bot.sendMessage(msg.from.id, createSentence(sentenceOptions));
    });
  } else {
    bot.sendMessage(msg.from.id, callOptions.sentence);
  }

});
