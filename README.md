# HASSSIO TELEGRAM BOT

## Installation

Clone the project and install dependencies with

```
npm i
```

## Config

The config.example.json has to be renamed config.json and the file requires the telegram bot token (tg_token), the home assistant token (hassio_token) and the url of your hassio (for example http://192.168.2.34:8123, WITHOUT "/" at the end).

## Launch

```
node server.js
```

## Use

Send messages to your bot !


## Grow up the bot

In the talk.json file you can add questions/answers with actions. Just write as the base structure is.
